﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

/// <summary>
/// A wrapper for the NetworkManager & now the main menu system.
/// </summary>
public class NewNetHUD : MonoBehaviour
{
	public NetworkManager myMan;
	public string ip;
	public string port;
	public static NewNetHUD instance;
	public GUIStyle style, style_tt;
	public bool ranServer = false;

	public void ExitTheFrigginGame()
	{
		if(ranServer)
		{
			NetworkManager.singleton.StopServer();
		}
		//else
		{
			NetworkManager.singleton.client.Disconnect();
		}
		UnityEngine.SceneManagement.SceneManager.LoadScene(0);
	}

	void Start()
	{
		instance = this;
		if(PlayerPrefs.HasKey("IPLast"))
		{
			ip = PlayerPrefs.GetString("IPLast");
		}
		if(PlayerPrefs.HasKey("PortLast"))
		{
			port = PlayerPrefs.GetString("PortLast");
		}
	}

	void SetPrefs()
	{
		PlayerPrefs.SetString("IPLast", ip);
		PlayerPrefs.SetString("PortLast", port);
	}

	void OnGUI()
	{
		if(!NetworkManager.singleton.isNetworkActive)
		{
			GUILayout.BeginArea(new Rect(25, 25, 150, 1080));
			GUILayout.Label("-IP-", style_tt);
			ip = GUILayout.TextField(ip, style);
			GUILayout.Label("-PORT-", style_tt);
			port = GUILayout.TextField(port, style);
			NetworkManager.singleton.useWebSockets = GUILayout.Toggle(NetworkManager.singleton.useWebSockets, "Web sockets?", style);
			NetworkManager.singleton.serverBindToIP = GUILayout.Toggle(NetworkManager.singleton.serverBindToIP, "Bind to IP?", style);
			GUILayout.Label("=CONNECTIONS=", style_tt);
			if(GUILayout.Button("Host?", style))
			{
				SetPrefs();
				NetworkManager.singleton.networkPort = int.Parse(port);
				NetworkManager.singleton.networkAddress = ip;
				NetworkManager.singleton.serverBindAddress = ip;
				NetworkManager.singleton.StartHost();
				ranServer = true;
			}
			if(GUILayout.Button("Connect?", style))
			{
				SetPrefs();
				NetworkManager.singleton.networkPort = int.Parse(port);
				NetworkManager.singleton.networkAddress = ip;
				NetworkManager.singleton.StartClient();
				ranServer = false;
			}
			GUILayout.EndArea();
		}
	}
}