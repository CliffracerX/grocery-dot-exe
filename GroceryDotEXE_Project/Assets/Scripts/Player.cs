﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Player : NetworkBehaviour
{
	public float moveSpeed,lookSpeed;
	public Rigidbody myBody;
	public Transform camera;
	public AudioSource musMain,musWarning,musVictoryDrums;
	public float lookRot = 0;
	public SyncListString itemsRequired, itemsAcquired;
	public SyncListInt itemsRequired_num, itemsAcquired_num;
	[SyncVar]
	public int bytesAvailable, startBytes;
	public UnityEngine.UI.Text checklist,healthbar_text;
	public UnityEngine.UI.Image healthbar_fg;
	[SyncVar]
	public float malmeter,malmeterMax;
	public GameObject shelf_prefab_for_list;
	public bool itemFailure;
	public Collider col;
	public System.DateTime startTime,endTime;
	public bool shoppingStarted = false, shoppingFinished = false;
	[SyncVar]
	public int infectedItems = 0;
	public Renderer renderer;
	public Material infectedMaterial;

	void OnTriggerEnter(Collider other)
	{
		if(other.tag == "Finish")
		{
			if(shoppingStarted && !shoppingFinished)
			{
				shoppingFinished = true;
			}
			else if(!shoppingFinished)
			{
				startTime = System.DateTime.Now;
				shoppingStarted = true;
			}
		}
	}

	void Start()
	{
		if(isServer)
		{
			//bytesAvailable = Random.Range(1500, 5000);
			Shelves sh = shelf_prefab_for_list.GetComponent<Shelves>();
			while(itemsRequired.Count <= 0)
			{
				for(int i = 0; i<Random.Range(3, 9); i++)
				{
					ShelfItemCategory cat = sh.categories[Random.Range(0, sh.categories.Length)];
					int itm = Random.Range(0, cat.names.Length);
					int numItms = Random.Range(cat.countMin[itm], cat.countMax[itm]);
					if(numItms > 0)
					{
						if(itemsRequired.Contains(cat.names[itm]))
						{
							itemsRequired_num.Add(numItms);
							for(int j = 0; j<numItms; j++)
							{
								bytesAvailable += Random.Range((int)(0.9f * cat.prices[itm]), (int)(1.5f * cat.prices[itm]));
							}
						}
						else
						{
							itemsRequired.Add(cat.names[itm]);
							itemsRequired_num.Add(numItms);
							for(int j = 0; j<numItms; j++)
							{
								bytesAvailable += Random.Range((int)(0.9f * cat.prices[itm]), (int)(1.5f * cat.prices[itm]));
							}
						}
					}
				}
			}
			for(int i = 0; i<itemsRequired.Count; i++)
			{
				itemsAcquired.Add(itemsRequired[i]);
				itemsAcquired_num.Add(0);
			}
			startBytes = bytesAvailable;
		}
		if(isLocalPlayer)
		{
			musMain.Play();
			musWarning.Play();
			musVictoryDrums.Play();
		}
		checklist = GameObject.Find("GUI_Checklist").GetComponent<UnityEngine.UI.Text>();
		healthbar_text = GameObject.Find("GUI_HealthbarText").GetComponent<UnityEngine.UI.Text>();
		healthbar_fg = GameObject.Find("GUI_Healthbar").GetComponent<UnityEngine.UI.Image>();
	}

	void FixedUpdate()
	{
		if(isLocalPlayer)
		{
			if(malmeter > 0)
			{
				Vector2 dir = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
				myBody.MovePosition(transform.position + (transform.forward*dir.y*Time.fixedDeltaTime*moveSpeed) + (transform.right*dir.x*Time.fixedDeltaTime*moveSpeed));
			}
			else
			{
				Cursor.visible = true;
				Cursor.lockState = CursorLockMode.None;
			}
		}
		else
		{
			camera.gameObject.SetActive(false);
		}
	}

	void Update()
	{
		Collider[] playersNearby = Physics.OverlapSphere(transform.position, 12);
			float mult = 0;
			float musMult = 0;
			for(int i = 0; i<playersNearby.Length; i++)
			{
				if(playersNearby[i].tag == "Player" && playersNearby[i] != col)
				{
					float dist = Vector3.Distance(transform.position, playersNearby[i].transform.position);
					if(dist <= 6) //Virtual Distancing failure zone!!
					{
						musMult += 1;
						mult += Mathf.Clamp(Mathf.Pow((1-(dist/6)), 2), 0, 999)*50;
					}
					else
					{
						musMult += Mathf.Clamp01(Mathf.Pow(1-((dist-6)/6), 2));
					}
				}
			}
			mult += (infectedItems * 1f);
			musMult += (infectedItems);
			musMult = Mathf.Clamp01(musMult);
		if(isServer && !shoppingFinished)
		{
			malmeter -= Time.deltaTime * mult;
		}
		if(malmeter <= 0)
		{
			shoppingFinished = true;
			malmeter = 0;
			mult = 0;
			musMult = 1;
			transform.localScale = new Vector3(1, 0.25f, 1);
			renderer.material = infectedMaterial;
		}
		if(isLocalPlayer)
		{
			lookRot = Mathf.Clamp(lookRot - (lookSpeed*Time.fixedDeltaTime*Input.GetAxis("Mouse Y")), -90, 90);
			camera.localEulerAngles = new Vector3(lookRot, 0, 0);
			transform.localEulerAngles = new Vector3(0, transform.localEulerAngles.y + (lookSpeed*Time.fixedDeltaTime*Input.GetAxis("Mouse X")), 0);
			musMain.volume = shoppingFinished ? 0 : 1;
			musWarning.volume = Mathf.Clamp01(musMult);
			musVictoryDrums.volume = Mathf.Clamp01(Mathf.Pow(1 - ((float)(bytesAvailable-(startBytes/2))/(float)(startBytes/2)), 2));
			healthbar_text.text = "Mal-Meter: 0x"+System.Convert.ToString((int)Mathf.Round(malmeter), 16).PadLeft(2, '0')+"/0x"+System.Convert.ToString((int)Mathf.Round(malmeterMax), 16).PadLeft(2, '0');
			healthbar_fg.gameObject.transform.localScale = new Vector3(Mathf.Floor(malmeter-0.5f)/malmeterMax, 1, 1);
			if(Input.GetButtonDown("TakeCare"))
			{
				this.CmdTakeCare();
			}
			if(malmeter <= 0)
			{
				checklist.text = "[ S C R O N C H E D]";
				checklist.text += "\n(Your antivirus has failed.)";
				checklist.text += "\n(Effectiveness: ";
				float eff = 0;
				for(int i = 0; i<itemsRequired.Count; i++)
				{
					eff += Mathf.Clamp01(itemsAcquired_num[i]/itemsRequired_num[i]);
				}
				eff /= itemsRequired.Count;
				eff *= 100;
				checklist.text += eff+"%)";
				checklist.text += "\nFinal tally of items: ";
				for(int i = 0; i<itemsAcquired.Count; i++)
				{
					checklist.text += "\n * "+itemsAcquired_num[i]+"x "+itemsAcquired[i];
				}
				//bear witness
				GameObject.Find("ExitButtonLose").transform.localScale = new Vector3(1, 1, 1);
			}
			else if(shoppingFinished == false)
			{
				checklist.text = "";
				if(mult > 0)
				{
					checklist.text = "==WARNING: MALWARE DETECTED!==";
					checklist.text += infectedItems > 0 ? "\nCarrying "+infectedItems+" piece"+(infectedItems > 1 ? "s" : "")+" of infected merchandise!" : "";
					checklist.text += "\nAntivirus failure in: "+GenPrettyAVTime(mult);
					checklist.text += "\nMalware will be cleared on facility exit - finish your shopping ASAP!";
					checklist.text += "\n\n";
				}
				if(bytesAvailable > 0)
				{
					checklist.text += "Bytes left: "+((""+bytesAvailable).PadLeft(4, '0'));
				}
				else
				{
					checklist.text += ((Time.time%1) > 0.5f) ? "==OUT OF BYTES!==" : "==             ==";
				}
				if(shoppingStarted)
				{
					endTime = System.DateTime.Now;
					checklist.text += "\nTime taken so far: "+GenPrettyTime();
				}
				itemFailure = false;
				for(int i = 0; i<itemsRequired.Count; i++)
				{
					checklist.text += "\n* "+itemsRequired[i]+": "+itemsAcquired_num[i]+"/"+itemsRequired_num[i]+" acquired";
					if(itemsAcquired_num[i] < itemsRequired_num[i])
					{
						itemFailure = true;
					}
				}
				if(!itemFailure)
				{
					checklist.text += "\n\nALL ITEMS ACQUIRED!";
				}
				checklist.text += "\n\n=EXTRAS=";
				for(int i = itemsRequired.Count; i<itemsAcquired.Count; i++)
				{
					checklist.text += "\n* "+itemsAcquired_num[i]+"x "+itemsAcquired[i];
				}
			}
			else
			{
				checklist.text = "=SHOPPING COMPLETED!=";
				checklist.text += "\nTook "+GenPrettyTime()+" to shop.";
				checklist.text += "\nSpent "+(startBytes - bytesAvailable)+" bytes.";
				if(infectedItems > 0)
				{
					checklist.text += "\nPurchased "+infectedItems+" infected items.";
				}
				if(malmeter < malmeterMax)
				{
					checklist.text += "\nAntivirus integrity dropped by "+(((malmeterMax - malmeter)/malmeterMax)*100).ToString("0.00")+"%.";
				}
				checklist.text += "\nEffectiveness: ";
				float eff = 0;
				for(int i = 0; i<itemsRequired.Count; i++)
				{
					eff += Mathf.Clamp01(itemsAcquired_num[i]/itemsRequired_num[i]);
				}
				eff /= itemsRequired.Count;
				eff *= 100;
				checklist.text += eff+"%";
				checklist.text += "\n\n=FINAL HAUL=";
				for(int i = 0; i<itemsAcquired.Count; i++)
				{
					checklist.text += "\n * "+itemsAcquired_num[i]+"x "+itemsAcquired[i];
				}
				checklist.text += "\n\n=THANK YOU FOR PLAYING!=";
				//to the most 0430 hack ever
				GameObject.Find("ExitButton").transform.localScale = new Vector3(1, 1, 1);
				Cursor.visible = true;
				Cursor.lockState = CursorLockMode.None;
			}
			if(Input.GetButtonDown("Fire1"))
			{
				Cursor.visible = false;
				Cursor.lockState = CursorLockMode.Locked;
				//TODO: do we make it so you can't buy extra items?
				this.CmdTryCollectItem(lookRot, true);
			}
			if(Input.GetKeyDown(KeyCode.Escape))
			{
				Cursor.visible = true;
				Cursor.lockState = CursorLockMode.None;
			}
		}
	}

	public string GenPrettyTime()
	{
		//this is ugly, don't use var, var is hideous and abombinable
		var time = (endTime - startTime);
		return Mathf.Floor((float)time.TotalSeconds/60f).ToString("00")+":"+(time.TotalSeconds%60).ToString("00")+":"+(time.TotalSeconds%1).ToString("0.00").Substring(2);
	}

	public string GenPrettyAVTime(float mult)
	{
		float secs = malmeter / mult;
		return Mathf.Floor((float)secs/60f).ToString("00")+":"+(secs%60).ToString("00")+":"+(secs%1).ToString("0.00").Substring(2);
	}

	[Command]
	public void CmdTakeCare()
	{
		this.RpcTakeCare();
	}

	public AudioSource takeCare;

	[ClientRpc]
	public void RpcTakeCare()
	{
		this.takeCare.Play();
	}

	[Command]
	public void CmdTryCollectItem(float lookRotNew, bool itemfail)
	{
		camera.localEulerAngles = new Vector3(lookRotNew, 0, 0);
		RaycastHit rh;
		if(Physics.Raycast(camera.position, camera.forward, out rh, 5))
		{
			if(rh.collider.GetComponent<CollectibleItem>() != null)
			{
				CollectibleItem ci = rh.collider.GetComponent<CollectibleItem>();
				if(bytesAvailable >= ci.price)
				{
					bytesAvailable -= ci.price;
					if(itemsAcquired.Contains(ci.name))
					{
						bool foundnum = false;
						for(int i = 0; i<itemsAcquired.Count; i++)
						{
							if(itemsAcquired[i].Equals(ci.name))
							{
								foundnum = true;
								itemsAcquired_num[i] = itemsAcquired_num[i] + 1;
								if(ci.isInfected)
								{
									infectedItems += 1;
								}
								ci.PickedUp();
							}
						}
						if(!foundnum)
						{
							itemsAcquired.Add(ci.name);
							itemsAcquired_num.Add(1);
							if(ci.isInfected)
							{
								infectedItems += 1;
							}
							ci.PickedUp();
						}
					}
					else
					{
						itemsAcquired.Add(ci.name);
						itemsAcquired_num.Add(1);
						if(ci.isInfected)
						{
							infectedItems += 1;
						}
						ci.PickedUp();
					}
				}
			}
		}
	}
}