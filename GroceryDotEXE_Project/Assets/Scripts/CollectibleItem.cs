﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class CollectibleItem : NetworkBehaviour
{
	public string name,description;
	public int price;
	public MeshRenderer[] rends;
	public int[] infectedSlots;
	public Material[] matInfected;
	[SyncVar]
	public bool isInfected;
	public bool updatedMats = false;

	void Update()
	{
		if(isInfected && !updatedMats)
		{
			updatedMats = true;
			for(int i = 0; i<rends.Length; i++)
			{
				rends[i].materials[infectedSlots[i]].color = matInfected[i].color;
				rends[i].materials[infectedSlots[i]].mainTexture = matInfected[i].mainTexture;
				rends[i].materials[infectedSlots[i]].SetColor("_EmissionColor", matInfected[i].GetColor("_EmissionColor"));
				rends[i].materials[infectedSlots[i]].SetTexture("_EmissionMap", matInfected[i].GetTexture("_EmissionMap"));
			}
		}
	}

	public void PickedUp()
	{
		if(isServer)
		{
			NetworkServer.Destroy(this.gameObject);
		}
	}
}