﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Cover : NetworkBehaviour
{
	[SyncVar]
	public int cat, catInd;
	public GameObject shelvesPrefab;
	public Shelves shelves;
	public Renderer renderer;
	public int matSlotID;
	public bool setMaterials = false;

	void Start()
	{
		shelves = shelvesPrefab.GetComponent<Shelves>();
	}

	void Update()
	{
		if(!setMaterials)
		{
			if(cat != -1 && catInd != -1)
			{
				if(shelves.categories[cat].covers[catInd] != null)
				{
					renderer.materials[matSlotID].mainTexture = shelves.categories[cat].covers[catInd];
					renderer.materials[matSlotID].SetTexture("_EmissionMap", shelves.categories[cat].covers[catInd]);
					setMaterials = true;
				}
			}
		}
	}

	public void Setup(int catIn, int catIndIn)
	{
		print("Setup called: "+catIn+", "+catIndIn);
		//if(isServer)
		{
			this.cat = catIn;
			this.catInd = catIndIn;
		}
	}
}