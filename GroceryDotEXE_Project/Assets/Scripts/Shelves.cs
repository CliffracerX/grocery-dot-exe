﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[System.Serializable]
public class RequiredItemCategory
{
	public string[] itemReqNames;
	public float[] itemReqChances;
	public int[] itemReqCountMin,itemReqCountMax;
}

[System.Serializable]
public class ShelfItemCategory
{
	public string name;
	public GameObject[] items;
	public int[] prices;
	public string[] names;
	[TextArea(3,10)]
	public string[] descriptions;
	public Texture2D[] covers;
	public float[] spawnChances,infectionChances;
	public int[] countMin,countMax;
}

public class Shelves : NetworkBehaviour
{
	public int catLeft, catRight;
	public int[] catIndLeft, catIndRight;
	public Transform[] spawnsLeft, spawnsRight;
	public TextMesh[] textsLeft,textsRight;
	public ShelfItemCategory[] categories;

	void Start()
	{
		if(isServer)
		{
			for(int i = 0; i<catIndLeft.Length; i++)
			{
				if(Random.Range(0f, 100f) <= categories[catLeft].spawnChances[catIndLeft[i]])
				{
					GameObject spawned = (GameObject)Instantiate(categories[catLeft].items[catIndLeft[i]], spawnsLeft[i].position, spawnsLeft[i].rotation);
					spawned.GetComponent<CollectibleItem>().name = categories[catLeft].names[catIndLeft[i]];
					spawned.GetComponent<CollectibleItem>().price = categories[catLeft].prices[catIndLeft[i]];
					if(Random.Range(0f, 100f) <= categories[catLeft].infectionChances[catIndLeft[i]])
					{
						spawned.GetComponent<CollectibleItem>().isInfected = true;
					}
					if(spawned.GetComponent<Cover>() != null)
					{
						//if(categories[catLeft].covers[catIndLeft[i]] != null)
						{
							spawned.GetComponent<Cover>().Setup(catLeft, catIndLeft[i]);
						}
					}
					NetworkServer.Spawn(spawned);
				}
				textsLeft[i].text = "="+categories[catLeft].names[catIndLeft[i]]+": "+categories[catLeft].prices[catIndLeft[i]]+"B=\n"+categories[catLeft].descriptions[catIndLeft[i]];
			}
			for(int i = 0; i<catIndRight.Length; i++)
			{
				if(Random.Range(0f, 100f) <= categories[catRight].spawnChances[catIndRight[i]])
				{
					GameObject spawned = (GameObject)Instantiate(categories[catRight].items[catIndRight[i]], spawnsRight[i].position, spawnsRight[i].rotation);
					spawned.GetComponent<CollectibleItem>().name = categories[catRight].names[catIndRight[i]];
					spawned.GetComponent<CollectibleItem>().price = categories[catRight].prices[catIndRight[i]];
					if(Random.Range(0f, 100f) <= categories[catRight].infectionChances[catIndRight[i]])
					{
						spawned.GetComponent<CollectibleItem>().isInfected = true;
					}
					if(spawned.GetComponent<Cover>() != null)
					{
						//if(categories[catRight].covers[catIndRight[i]] != null)
						{
							spawned.GetComponent<Cover>().Setup(catRight, catIndRight[i]);
						}
					}
					NetworkServer.Spawn(spawned);
				}
				textsRight[i].text = "="+categories[catRight].names[catIndRight[i]]+": "+categories[catRight].prices[catIndRight[i]]+"B=\n"+categories[catRight].descriptions[catIndRight[i]];
			}
		}
	}

	void Update()
	{
		for(int i = 0; i<catIndLeft.Length; i++)
		{
			textsLeft[i].text = "="+categories[catLeft].names[catIndLeft[i]]+": "+categories[catLeft].prices[catIndLeft[i]]+"B=\n"+categories[catLeft].descriptions[catIndLeft[i]];
		}
		for(int i = 0; i<catIndRight.Length; i++)
		{
			textsRight[i].text = "="+categories[catRight].names[catIndRight[i]]+": "+categories[catRight].prices[catIndRight[i]]+"B=\n"+categories[catRight].descriptions[catIndRight[i]];
		}
	}
}