﻿//Sourced from: http://blog.runtimedevelopment.com/index.php/2017/02/17/text-occlusion-in-unity/
//We would just use TextMeshPro except the font atlas generator has no Linux plugin because...heck if we know.
//C'mon, people, Unity officially supports Linux and has for years now, TMP is an official part of the engine, you could stand to bloody include support for it .-.
Shader "GUI/3D Text Shader" { 
	Properties {
		_MainTex ("Font Texture", 2D) = "white" {}
		_Color ("Text Color", Color) = (1,1,1,1)
	}
 
	SubShader {
		Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
		Lighting Off Cull Off ZWrite Off Fog { Mode Off }
		Blend SrcAlpha OneMinusSrcAlpha
		Pass {
			Color [_Color]
			SetTexture [_MainTex] {
				combine primary, texture * primary
			}
		}
	}
}