## GROCERY DOR EXE: A Game About Virtual Pandemic Shopping
* The year is 21XX.  Tens of dueling pandemics ravage the world, and most interaction has moved to The Information Superhighway.
* A recent development complicates things further: the Info-Realm, a virtual reality created by users of the ISH is coming down with a pandemic of nasty malware to boot!
* You need to make a run to the digital grocery store to select goods to be materialised at your house - virtually distance and avoid getting infected!  If the virus gets you, your computer's fried & you'll need to call tech support!

### MECHANICS
* Virtually distance!
  * Even if you know you're not infected, you can't know if anyone else is free of contamination or not!
  * Maintain a 2 meter distance at all times!  You lose points for staying too close, and will be booted from the store for risk of virus transmission if you stay for too long!
* Communicate!
  * READ: A vocoded "TAKE CARE" you can access by pressing V at any time.
* Shop & try to get the resources you need before the store's sold out!
  * You need things!  They need things!  Everybody needs things!  If you're unlucky, your shopping list may exhaust the store!
  * Furthermore, some items may be contaminated, meaning you'll have to carefully work around them - if you pick one up by accident, the malware might spread to your avatar!

### MVP:
* Step 1: Beans in multiplayer - DONE
  * Network HUD - DONE
  * Movement, mouselook, and mouse capture - DONE
* Step 2: Shelves w/ collectible items - DONE
  * Basic building mockup w/ shelves containing item spawn points - DONE
  * Colored cubes w/ floating ID text spawn at points - DONE
  * Ability to collect cubes - DONE
* Step 3: Random list of items to collect + victory condition (get all the items, pay, and get out) - DONE
  * Start with a list of items you need and a certain amount of Bytes (money) - DONE
  * Collecting a cube adds it to a list of collected items, giving a read-out of how many Bytes it'll cost - DONE
  * Check-out lets you cash in, spending your bytes & rating how effective you were at getting what you needed - DONE
    * This is being changed (for now) to a simple system whereby once you exit the store, you're told how effective you were (% of items acquired), how many Bytes you spent to do it, and your time.
* Step 4: Infected items, Virtually Distanced Meter - DONE
  * Being near other players drains your Antivirus meter, being within 5m of them turns the music on quietly, ramping up exponentially until it maxes out at full volume within 2m. - DONE
  * You lose exponentially more AV the closer you get, maxing out at 0.5m where the bar will drain fully in five seconds. - DONE
  * Items may be Infected when spawned - changes their color to red & will drain your Antivirus meter whenever you touch it, as well as spreading Infectedness to nearby objects (slowly dithering them from green to yellow to red) - DONE
  * If you can survive collecting & being near them, the checkout is a perfect and super-powerful antivirus and so you *can* succeed with only infected ones, however it penalizes your score - DONE
  * If your antivirus meter runs out, it's game over!  Lost AV also penalizes your final score. - \[SCRONCHED\]
* Step 5: Proper Stylised Item Labels - DONE
  * This means putting proper labels on the shelves you can inspect & read, as well as starting to add distinct visuals for different kinds of items - LABELS DONE
  * This will be the most content heavy thing to work on - SOME CONTENT IS DONE
* Step 6: Style Pass - IGNORING
  * This likely means shaders more or less ripping off the look of Return of the Obra Dinn but for use of CGA Palette 0, High-Intensity
  * Objects will mostly be green or red, with yellow highlights and outlines
* Step 7: Super Vaporwave Playermodels
  * You're a bean with color-selectable cool cyberpunk shades...without the color selection. - DONE
* Step 8: TAKE CARE - DONE
  * Press F to Take Care - DONE